package snake;

import javax.swing.*;
import java.util.Random;

public class GameData {
    private int id;
    //蛇的长度
    private int length;
    private int[] snakeX;//蛇的x坐标
    private int[] snakeY;//蛇的Y坐标
    private String fx;//小蛇的方向
    //食物的位置
    private int[] foodX;
    private int[] foodY;
    //糖果的位置
    private int[] candyX;
    private int[] candyY;
    //炸弹
    private int[] bombX;
    private int[] bombY;

    public GameData() {
    }

    public GameData(int length, int[] snakeX, int[] snakeY, String fx, int[] foodX, int[] foodY, int[] candyX, int[] candyY, int[] bombX, int[] bombY, boolean isFail, boolean isStart, int score, String str, int strX, int strY, int delay) {
        this.length = length;
        this.snakeX = snakeX;
        this.snakeY = snakeY;
        this.fx = fx;
        this.foodX = foodX;
        this.foodY = foodY;
        this.candyX = candyX;
        this.candyY = candyY;
        this.bombX = bombX;
        this.bombY = bombY;
        this.isFail = isFail;
        this.isStart = isStart;
        this.score = score;
        this.str = str;
        this.strX = strX;
        this.strY = strY;
        this.delay = delay;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        if (length>0){
            this.length = length;
        }else {
            this.length = 3;
        }

    }

    public int[] getSnakeX() {
        return snakeX;
    }

    public void setSnakeX(int[] snakeX) {
        this.snakeX = snakeX;
    }

    public int[] getSnakeY() {
        return snakeY;
    }

    public void setSnakeY(int[] snakeY) {
        this.snakeY = snakeY;
    }

    public String getFx() {
        return fx;
    }

    public void setFx(String fx) {
        this.fx = fx;
    }

    public int[] getFoodX() {
        return foodX;
    }

    public void setFoodX(int[] foodX) {
        this.foodX = foodX;
    }

    public int[] getFoodY() {
        return foodY;
    }

    public void setFoodY(int[] foodY) {
        this.foodY = foodY;
    }

    public int[] getCandyX() {
        return candyX;
    }

    public void setCandyX(int[] candyX) {
        this.candyX = candyX;
    }

    public int[] getCandyY() {
        return candyY;
    }

    public void setCandyY(int[] candyY) {
        this.candyY = candyY;
    }

    public int[] getBombX() {
        return bombX;
    }

    public void setBombX(int[] bombX) {
        this.bombX = bombX;
    }

    public int[] getBombY() {
        return bombY;
    }

    public void setBombY(int[] bombY) {
        this.bombY = bombY;
    }

    public boolean isFail() {
        return isFail;
    }

    public void setFail(boolean fail) {
        isFail = fail;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        isStart = start;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public int getStrX() {
        return strX;
    }

    public void setStrX(int strX) {
        this.strX = strX;
    }

    public int getStrY() {
        return strY;
    }

    public void setStrY(int strY) {
        this.strY = strY;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }


    //游戏状态
    private boolean isFail;//游戏是否失败
    private boolean isStart;//游戏是否开始

    //游戏得分
    private int score;
    //分数变化特效(不用保存为游戏数据)
    private String str = "";
    private int strX = 0;
    private int strY = 0;

    //创建一个计时器，延迟一段delay毫秒就执行第一次任务
    private int delay;
}