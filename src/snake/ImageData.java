package snake;

import javax.swing.*;
import java.net.URL;
//游戏的图片数据
public class ImageData {
    public static URL bodyURL = ImageData.class.getResource("statics/body.png");
    public static URL downURL = ImageData.class.getResource("statics/down.png");
    public static URL foodURL = ImageData.class.getResource("statics/food.png");
    public static URL leftURL = ImageData.class.getResource("statics/left.png");
    public static URL rightURL = ImageData.class.getResource("statics/right.png");
    public static URL upURL = ImageData.class.getResource("statics/up.png");
    public static URL headerURL = ImageData.class.getResource("statics/header.png");
    public static URL candyURL = ImageData.class.getResource("statics/candy.png");
    public static URL bombURL = ImageData.class.getResource("statics/bomb.jpg");

    public static ImageIcon body = new ImageIcon(bodyURL);
    public static ImageIcon down = new ImageIcon(downURL);
    public static ImageIcon food = new ImageIcon(foodURL);
    public static ImageIcon left = new ImageIcon(leftURL);
    public static ImageIcon right = new ImageIcon(rightURL);
    public static ImageIcon up = new ImageIcon(upURL);
    public static ImageIcon header = new ImageIcon(headerURL);
    public static ImageIcon candy = new ImageIcon(candyURL);
    public static ImageIcon bomb = new ImageIcon(bombURL);



}
