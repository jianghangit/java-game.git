package snake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class StartGame{
    public static void main(String[] args) {
        JFrame jFrame = new JFrame("李江江的贪吃蛇");
        jFrame.setBounds(10,10,920,800);

        jFrame.setResizable(false);
        jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        GamePanel gamePanel = new GamePanel();
        jFrame.add(gamePanel);

        jFrame.setVisible(true);
        jFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new MyDialog(gamePanel);
            }
        });
    }
        private static class  MyDialog extends JDialog{
            public MyDialog(GamePanel gamePanel){
                this.setBounds(800,400,200,200);
                this.setLayout(null);
                this.setVisible(true);
                JLabel jLabel = new JLabel("是否保存游戏？");
                jLabel.setHorizontalAlignment(JLabel.CENTER);
                jLabel.setBounds(0,0,200,100);
                JButton button1 = new JButton("是");
                button1.setBounds(30,100,50,50);
                JButton button2 = new JButton("否");
                button2.setBounds(100,100,50,50);
                button1.addActionListener(e -> {
                    //保存游戏到数据库
                    GameData gameData = gamePanel.getGameData();
                    //笔者并不想添加次功能，有兴趣的小伙伴可以自行添加
                    //调用JDBC保存
                    //System.out.println(gameData.getScore());
                    System.exit(0);
                });
                button2.addActionListener(e->{
                    System.exit(0);
                });
                this.add(jLabel);
                this.add(button1);
                this.add(button2);
            }
        }
}
